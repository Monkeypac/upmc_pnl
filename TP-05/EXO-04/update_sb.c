#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/timekeeping.h>
#include <uapi/linux/time.h>

MODULE_DESCRIPTION("Module show super block");
MODULE_AUTHOR("Thomas Magalhaes, UPMC");
MODULE_LICENSE("GPL");

extern void iterate_supers(void (*)(struct super_block *, void *), void *);

static char *fs_name = "ext4";
module_param(fs_name, charp, 0755);

static void print_sb(struct super_block *sb, void*a) {
	struct timespec time = sb->s_last_accessed;
	pr_info("uuid=%pUB type=%s time=%ld,%ld\n", sb->s_uuid, sb->s_type->name, time.tv_sec, time.tv_nsec);
	getnstimeofday(&sb->s_last_accessed);
}

static int mod_init(void)
{
	struct file_system_type* fs = get_fs_type(fs_name);
	iterate_supers_type(fs, print_sb, NULL);
	put_filesystem(fs);
	return 0;
}
module_init(mod_init);

static void mod_exit(void)
{
}
module_exit(mod_exit);

