#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>

MODULE_DESCRIPTION("Module show super block");
MODULE_AUTHOR("Thomas Magalhaes, UPMC");
MODULE_LICENSE("GPL");

extern void iterate_supers(void (*)(struct super_block *, void *), void *);

static void print_sb(struct super_block *sb, void*a) {
	pr_info("uuid=%pUB type=%s\n", sb->s_uuid, sb->s_type->name);
}

static int mod_init(void)
{
	iterate_supers(print_sb, NULL);
	return 0;
}
module_init(mod_init);

static void mod_exit(void)
{
}
module_exit(mod_exit);

