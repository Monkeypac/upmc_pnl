#include<stdlib.h>
#include<stdio.h>

#include "commit.h"
#include "version.h"

int main(int argc, char const *argv[])
{
    struct version v =
                {.major = 3,
                .minor = 5,
                .flags = 0};

    char *comment = (char*)malloc(10);
    sprintf(comment, "Bonjour!");

    struct commit c = {.id = 1, .version = v, .comment=comment, .next = NULL, .prev = NULL };

    printf("Commit (%ld)\n\t", c.id);
    display_version(&c.version, is_unstable_bis);
    printf("\t%s\n", c.comment);

    printf("sizeof(struct commit):%ld\n", sizeof(struct commit));

    printf("Commit:\t\t%x\n", &c);
    printf("id:\t\t%x\n", &c.id);
    printf("version:\t%x\n", &c.version);
    printf("comment:\t%x\n", &c.comment);
    printf("next:\t\t%x\n", &c.next);
    printf("prev:\t\t%x\n", &c.prev);

    printf("commitOf:\t%x %s\n", commitOf(&c.version), (commitOf(&c.version) == &c) ? "Valide" : "Non valide");

    free(comment);
    return 0;
}
