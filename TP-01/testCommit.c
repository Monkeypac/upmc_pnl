#include<stdlib.h>
#include<stdio.h>

#include"commit.h"

int main(int argc, char const* argv[])
{
    struct commit *first = new_commit(0, 0, "First !");
    struct history* h = new_history("Tout une histoire", first);
	struct commit *tmp, *victim, *last;

	display_commit(first);
	printf("\n");

    display_history(h);

	tmp = add_minor_commit(first, "Work 1");
	tmp = add_minor_commit(tmp, "Work 2");
	victim = add_minor_commit(tmp, "Work 3");
	last = add_minor_commit(victim, "Work 4");
    display_history(h);
 
	del_commit(victim);
    display_history(h);

	tmp = add_major_commit(last, "Realse 1");
	tmp = add_minor_commit(tmp, "Work 1");
	tmp = add_minor_commit(tmp, "Work 2");
	tmp = add_major_commit(tmp, "Realse 2");
	tmp = add_minor_commit(tmp, "Work 1");
    display_history(h);

	add_minor_commit(last, "Oversight !!!");
    display_history(h);

    infos(h, 1, 2);

    infos(h, 1, 7);

    infos(h, 4, 2);

	return 0;
}
