#include<stdlib.h>
#include<stdio.h>

#include"commit.h"

static int nextId = 0;

/**
  * new_commit - alloue et initialise une structure commit correspondant au parametre
  * @major:      numero de version majeure
  * @minor:      numero de version mineure
  * @comment:    pointeur vers une chaine de caracteres contenant un commentaire
  */
struct commit *new_commit(unsigned short major, unsigned long minor, char *comment)
{
    struct commit * c = (struct commit *)malloc(sizeof(struct commit));

    struct version v = {.major=major, .minor=minor, .flags=0};

    c->id = nextId++;
    c->comment = comment;
    c->version = v;
    c->next = NULL;
    c->prev = NULL;

    return c;
}

struct history *new_history(char *name, struct commit* start)
{
    struct history* h = (struct history*)malloc(sizeof(struct history));

    h->name = name;
    h->commit_list = start;
    h->commit_count = 0;

    return h;
}

struct commit *last_commit(struct history* h)
{
    struct commit* c;
    for(c = h->commit_list; c->next != NULL; c = c->next)
        ;

    return c;
}

/**
  * insert_commit - insert sans le modifier un commit dans la liste doublement chainee
  * @from:       commit qui deviendra le predecesseur du commit insere
  * @new:        commit a inserer - seul ses champs next et prev seront modifie
  */
static struct commit *insert_commit(struct commit *from, struct commit *new)
{
    if(from->next != NULL){
        new->next = from->next;
        from->next->prev = new;
    }
    new->prev = from;
    from->next = new;

    return new;
}

/**
  * add_minor_commit - genere et insert un commit correspondant a une version mineure
  * @from:           commit qui deviendra le predecesseur du commit insere
  * @comment:        commentaire du commit
  */
struct commit *add_minor_commit(struct commit *from, char *comment)
{
    struct commit* c = new_commit(from->version.major, from->version.minor+1, comment);
  return insert_commit(from, c);
}

/**
  * add_major_commit - genere et insert un commit correspondant a une version majeure
  * @from:           commit qui deviendra le predecesseur du commit insere
  * @comment:        commentaire du commit
  */
struct commit *add_major_commit(struct commit *from, char *comment)
{
    struct commit* c = new_commit(from->version.major+1, 0, comment);
  return insert_commit(from, c);
}

/**
  * del_commit - extrait le commit de l'historique
  * @victim:         commit qui sera sorti de la liste doublement chainee
  */
struct commit *del_commit(struct commit *victim)
{
	/* TODO : Exercice 3 - Question 5 */
  return NULL;
}

/**
  * display_commit - affiche un commit : "2:  0-2 (stable) 'Work 2'"
  * @c:             commit qui sera affiche
  */
void display_commit(struct commit *c)
{
    printf("%ld\t", c->id);
    display_version(&c->version, is_unstable_bis);
    printf("\t%s\n", c->comment);
}

/**
  * display_history - affiche tout l'historique, i.e. l'ensemble des commits de la liste
  * @from:           premier commit de l'affichage
  */
void display_history(struct history *h)
{
    printf("Historique de \'%s\' :\n", h->name);
    struct commit* c;
    for(c = h->commit_list->next; c != NULL; c = c->next)
        display_commit(c);
    printf("\n");
}

/**
  * infos - affiche le commit qui a pour numero de version <major>-<minor>
  * @major: major du commit affiche
  * @minor: minor du commit affiche
  */
void infos(struct history *h, int major, unsigned long minor)
{
    struct commit* c;
    for(c = h->commit_list->next; c != NULL; c = c->next)
        if(c->version.major == major && c->version.minor == minor)
            break;
    printf("Recherche du commit %d.%ld :\t", major, minor);
    if(c)
        display_commit(c);
    else
        printf("Not here !!!\n");

    printf("\n");
}

/**
  * commitOf - retourne le commit qui contient la version passe en parametre
  * @version:  pointeur vers la structure version dont cherche le commit
  * Note:      cette fonction continue de fonctionner meme si l'on modifie
  *            l'ordre et le nombre des champs de la structure commit.
  */
struct commit *commitOf(struct version *version)
{
    return (struct commit*)((void*)version - ((void*)&((struct commit*)0)->version));
}
