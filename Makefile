info_login="Login root nopasswd"
cpu_nb_cores=`cat /proc/cpuinfo | grep processor | wc -l`
info_cpu_nb_cores="NB_CORES="${cpu_nb_cores}

SRC_DIR=/tmp/linux-4.2.3

all: init_all make_all

#INIT
init_all: init_emu init_sources
init_emu:
	cp /usr/data/sopena/nmv/nmv-tp.img /tmp
	dd bs=1M count=50 if=/dev/zero of=./myHome.img
	/sbin/mkfs.ext4 -F ./myHome.img
init_sources:
	tar -xvJf /usr/data/sopena/nmv/linux-4.2.3.tar.xz -C /tmp/
	cp /usr/data/sopena/nmv/linux-config-nmv /tmp/linux-4.2.3/.config
	
#MAKE
make_all: make_sources infos_image
make_sources:
	make -C ${SRC_DIR} -j 16
	
#RUN
run: run_external
run_normal:
	bash qemu-run.sh
run_external:
	bash qemu-run-externKernel.sh

#INFOS
infos_image:
	file ${SRC_DIR}/arch/x86/boot/bzImage
infos:
	@echo ${info_login}
	@echo ${info_cpu_nb_cores}
