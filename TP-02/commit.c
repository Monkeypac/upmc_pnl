#include<stdlib.h>
#include<stdio.h>

#include"commit.h"

static int nextId = 0;

/**
  * new_commit - alloue et initialise une structure commit correspondant au parametre
  * @major:      numero de version majeure
  * @minor:      numero de version mineure
  * @comment:    pointeur vers une chaine de caracteres contenant un commentaire
  */
struct commit *new_commit(unsigned short major, unsigned long minor, char *comment)
{
    struct commit * c = (struct commit *)malloc(sizeof(struct commit));

    struct version v = {.major=major, .minor=minor, .flags=0};

    c->id = nextId++;
    c->comment = comment;
    c->version = v;
    INIT_LIST_HEAD(&c->list);
    INIT_LIST_HEAD(&c->major_list);
    c->major_parent=c;

    c->ops.display = (minor == 0) ? display_major_commit : display_commit;
    c->ops.extract = (minor == 0) ? extract_major : extract_minor;

    return c;
}

struct history *new_history(char *name, struct commit* start)
{
    struct history* h = (struct history*)malloc(sizeof(struct history));

    h->name = name;
    h->commit_list = start;
    h->commit_count = 0;

    return h;
}

struct commit *last_commit(struct history* h)
{
    /*struct commit* c;
    for(c = h->commit_list; c->next != NULL; c = c->next)
        ;

    return c;*/

    return list_last_entry(&h->commit_list->list, struct commit, list);
}

/**
  * insert_commit - insert sans le modifier un commit dans la liste doublement chainee
  * @from:       commit qui deviendra le predecesseur du commit insere
  * @new:        commit a inserer - seul ses champs next et prev seront modifie
  */
static struct commit *insert_commit(struct commit *from, struct commit *new)
{
    /*if(from->next != NULL){
        new->next = from->next;
        from->next->prev = new;
    }
    new->prev = from;
    from->next = new;*/

    list_add(&new->list, &from->list);

    //Version majeure
    if(new->version.minor == 0) {
        //Inserer dans major_list de from.major_parent
        list_add(&new->major_list, &from->major_parent->major_list);
        new->major_parent = new;
    }
    else {
        //Trouver le parent
        new->major_parent = from->major_parent;
    }

    return new;
}

/**
  * add_minor_commit - genere et insert un commit correspondant a une version mineure
  * @from:           commit qui deviendra le predecesseur du commit insere
  * @comment:        commentaire du commit
  */
struct commit *add_minor_commit(struct commit *from, char *comment)
{
    struct commit* c = new_commit(from->version.major, from->version.minor+1, comment);
    return insert_commit(from, c);
}

/**
  * add_major_commit - genere et insert un commit correspondant a une version majeure
  * @from:           commit qui deviendra le predecesseur du commit insere
  * @comment:        commentaire du commit
  */
struct commit *add_major_commit(struct commit *from, char *comment)
{
    struct commit* c = new_commit(from->version.major+1, 0, comment);
    return insert_commit(from, c);
}

/**
  * display_commit - affiche un commit : "2:  0-2 (stable) 'Work 2'"
  * @c:             commit qui sera affiche
  */
void display_commit(struct commit *c)
{
    printf("%ld\t", c->id);
    display_version(&c->version, is_unstable_bis);
    printf("\t%s\n", c->comment);
}

void display_major_commit(struct commit *c)
{
    printf("%ld\t### ", c->id);
    printf("version %d :", c->version.major);
    printf("\t%s ###\n", c->comment);
}

/**
  * display_history - affiche tout l'historique, i.e. l'ensemble des commits de la liste
  * @from:           premier commit de l'affichage
  */
void display_history(struct history *h)
{
    printf("Historique de \'%s\' :\n", h->name);
    struct commit* c;
    /*for(c = h->commit_list->next; c != NULL; c = c->next)
        display_commit(c);*/
    c = h->commit_list;
    c->ops.display(c);
    list_for_each_entry(c, &h->commit_list->list, list)
            c->ops.display(c);

    printf("\n");
}

/**
  * infos - affiche le commit qui a pour numero de version <major>-<minor>
  * @major: major du commit affiche
  * @minor: minor du commit affiche
  */
void infos(struct history *h, int major, unsigned long minor)
{
    struct commit* c, *d;
    /*for(c = h->commit_list->next; c != NULL; c = c->next)
        if(c->version.major == major && c->version.minor == minor)
            break;*/
    list_for_each_entry(c, &h->commit_list->major_parent->major_list, major_list)
            if(c->version.major == major)
            break;

    printf("Recherche du commit %d.%ld :\t", major, minor);

    if(c != h->commit_list) {
        list_for_each_entry(d, &c->list, list)
                if(d->version.minor == minor)
                break;

        if(d != c)
            d->ops.display(d);
        else
            printf("Not here !!!\n");
    }
    else {
        printf("Not here !!!\n");
    }

    printf("\n");
}

/**
  * commitOf - retourne le commit qui contient la version passe en parametre
  * @version:  pointeur vers la structure version dont cherche le commit
  * Note:      cette fonction continue de fonctionner meme si l'on modifie
  *            l'ordre et le nombre des champs de la structure commit.
  */
struct commit *commitOf(struct version *version)
{
    return (struct commit*)((void*)version - ((void*)&((struct commit*)0)->version));
}

/**
  * del_commit - extrait le commit de l'historique
  * @victim:         commit qui sera sorti de la liste doublement chainee
  */
struct commit *del_commit(struct commit *victim)
{
    return victim->ops.extract(victim);
}

struct commit *extract_minor(struct commit *victim)
{
    list_del(&victim->list);
    return victim;
}

struct commit *extract_major(struct commit *victim)
{
    struct commit *c, *safe;
    //On free tous les elements de la liste
    list_for_each_entry_safe(c, safe, &victim->list, list) {
        if(c->version.major == victim->version.major) {
            c = del_commit(c);
            free(c);
        }
        else {
            break;
        }
    }

    list_del(&victim->list);
    list_del(&victim->major_list);
    return victim;
}

void freeHistory(struct history* from)
{
    struct commit *c, *safe;
    //On free tous les elements de la liste
    list_for_each_entry_safe(c, safe, &from->commit_list->major_list, major_list) {
        del_commit(c);
        free(c);
    }

    //On free le premier element "fantome" de la liste
    free(from->commit_list);

    //On free l'history
    free(from);
}
