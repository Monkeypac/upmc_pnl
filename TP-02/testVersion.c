#include<stdlib.h>
#include<stdio.h>

#include"version.h"

int main(int argc, char const *argv[])
{
    struct version v = {.major = 3,
                .minor = 5,
                .flags = 0};

    int (*is_unstable_func)(struct version *) = is_unstable_bis;

    display_version(&v, is_unstable_func);
    printf("\n");

    v.minor++;
    display_version(&v, is_unstable_func);
    printf("\n");

    v.major++;
    v.minor = 0;
    display_version(&v, is_unstable_func);
    printf("\n");

    v.minor++;
    display_version(&v, is_unstable_func);
    printf("\n");

    printf("sizeof(struct version):%ld\n", sizeof(struct version));

	return 0;
}
